package com.mindgamematlab.tutorial2018;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Ajay on 3/19/2018.
 */

class Menus_singleton {
    private static final Menus_singleton ourInstance = new Menus_singleton();
    public JSONArray menus_json = new JSONArray();
    public JSONObject menus_data = new JSONObject();
    public int topic_index;
    public String ASSET_STRING = "file:///android_asset/";


    static Menus_singleton getInstance() {



        return ourInstance;
    }

    private Menus_singleton() {



    }
    /* Returns the String array list of menu titles for the given string*/
public ArrayList<String> get_menu_titles(){
    
    ArrayList<String> menu_title_array = new ArrayList<>();
    JSONArray jarray2 = new JSONArray();
    
    try {


        jarray2 = menus_data.getJSONArray("menus");

        for (int i = 0; i < jarray2.length(); i++) {
            menu_title_array.add(jarray2.getString(i));
        }
        


    }
    catch (Exception e){
        e.printStackTrace();
    }

    return menu_title_array;
}

    /* Returns the String array list of menu titles for the given string*/
public String get_menu_file(int pos){

    String menu_title_file = "";
    JSONArray jarray2 = new JSONArray();

    try {



         jarray2 = menus_data.getJSONArray("files");

        menu_title_file = ASSET_STRING + jarray2.getString(pos);

    }
    catch (Exception e){
        e.printStackTrace();
    }

    return menu_title_file;
}

/*Return the Topic*/
public String get_topic(int pos){
    String menu_topic = "";

    try {

        menu_topic = menus_data.getString("topic_title");

    }

    catch (Exception e){
        e.printStackTrace();
    }

    return menu_topic;
}

}
