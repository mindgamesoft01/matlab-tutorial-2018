package com.mindgamematlab.tutorial2018;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Date;


/**
 * Created by kalyani on 9/5/2017.
 */

public class Webview_Activity extends Activity {
    String[]html= {"file:///android_asset/1.htm", "file:///android_asset/2.htm", "file:///android_asset/3.htm", "file:///android_asset/4.htm", "file:///android_asset/5.htm", "file:///android_asset/6.htm", "file:///android_asset/7.htm", "file:///android_asset/8.htm", "file:///android_asset/9.htm", "file:///android_asset/10.htm", "file:///android_asset/11.htm", "file:///android_asset/12.htm", "file:///android_asset/13.htm", "file:///android_asset/14.htm", "file:///android_asset/15.htm", "file:///android_asset/16.htm", "file:///android_asset/17.htm", "file:///android_asset/18.htm", "file:///android_asset/19.htm", "file:///android_asset/20.htm", "file:///android_asset/21.htm", "file:///android_asset/22.htm", "file:///android_asset/23.htm", "file:///android_asset/24.htm", "file:///android_asset/25.htm", "file:///android_asset/26.htm", "file:///android_asset/27.htm", "file:///android_asset/28.htm", "file:///android_asset/29.htm"};

    WebView webView;
    int position;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    int scale;

    singleton_images sc= singleton_images.getInstance();
    InterstitialAd interstitialAd;
    String AD_UNIT_ID_full_page = "ca-app-pub-4951445087103663/9532037956";
    InterstitialAd interstitialAd1;
    String TAG="AD STATUS";
    String AD_UNIT_ID_full_page1 = "ca-app-pub-4951445087103663/9532037956";
     ProgressDialog pd ;
    NetworkStatusCheck NC= NetworkStatusCheck.getInstance();
    final AdRequest adRequest = new AdRequest.Builder().build();
    String app_status = sc.app_mode();
    TextView heading;

    Menus_singleton Mnu = Menus_singleton.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        webView=(WebView)findViewById(R.id.webview);
        heading=(TextView)findViewById(R.id.heading_text);
        WebSettings setting = webView.getSettings();

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);
        pd=new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Please wait ..." + "Add Loading ...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);


        interstitialAd = new InterstitialAd(this);
        interstitialAd1 = new InterstitialAd(this);


        interstitialAd.setAdUnitId(AD_UNIT_ID_full_page);

        final AdRequest adRequest1 = new AdRequest.Builder().build();
        interstitialAd1.setAdUnitId(AD_UNIT_ID_full_page1);







        setting.setJavaScriptEnabled(true);
        setting.setBuiltInZoomControls(true);
        setting.setSupportZoom(true);
        webView.setInitialScale(0);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        this.webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webView.setScrollbarFadingEnabled(false);
        Bundle b= getIntent().getExtras();
        position=b.getInt("POS");
        webView.loadUrl(Mnu.get_menu_file(position));
        heading.setText(Mnu.get_topic(0));


    }




        @Override
        public boolean onKeyDown ( int keyCode, KeyEvent event) {
//            if (app_status.equals("PROD")) {
//        super.onBackPressed();
                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                    Toast.makeText(getApplicationContext(), "Please connect to the internet", Toast.LENGTH_LONG).show();
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                    pd.dismiss();
                }
                if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                    Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                    pd.show();
                    Log.e(TAG, ad1_status.toString());
                    if (ad1_status == Boolean.FALSE) {
                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(i);
                        pd.dismiss();


                    }


                    if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                        interstitialAd.setAdListener(new AdListener() {
                            @Override
                            public void onAdClosed() {
                                super.onAdClosed();
                                Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(i);
                                pd.dismiss();


                            }

                            @Override
                            public void onAdFailedToLoad(int i) {
                                super.onAdFailedToLoad(i);

                                if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                    //cd.show();
                                    pd.dismiss();
                                }
                                Intent j = new Intent(getApplicationContext(), MainActivity.class);
                                startActivity(j);
                                pd.dismiss();

                                //set the last loaded timestamp even if the ad load fails
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                            }

                            @Override
                            public void onAdLeftApplication() {
                                super.onAdLeftApplication();
                            }

                            @Override
                            public void onAdOpened() {
                                super.onAdOpened();
                            }

                            @Override
                            public void onAdLoaded() {
                                super.onAdLoaded();
                                //set the last loaded timestamp
                                singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                interstitialAd.show();
                                pd.dismiss();

                            }
                        });
                        interstitialAd.loadAd(adRequest);
                    }
                }

//            }
            return super.onKeyDown(keyCode, event);
        }
    }



