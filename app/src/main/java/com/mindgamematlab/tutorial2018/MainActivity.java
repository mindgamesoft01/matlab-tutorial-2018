package com.mindgamematlab.tutorial2018;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;


public class MainActivity extends Activity {
    RecyclerView recyclerView;

    String val;
    TextView heading;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    Button btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10,btn11,btn12,btn13;


    Boolean connected;


    TextView terms_privacy, Heading;

    ScrollView scrollView;
    public static String []array_items;
    NetworkStatusCheck NC= NetworkStatusCheck.getInstance();
    singleton_images sc=singleton_images.getInstance();
    String app_status = sc.app_mode();
    Menus_singleton Mnu = Menus_singleton.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Bundle b=getIntent().getExtras();
//        val=b.getString("flag");

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);



        initViews();
        if (app_status.equals("TEST")) {

            recyclerView.setVisibility(View.INVISIBLE);

        }
        if (app_status.equals("PROD"))

        {
            recyclerView.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), 1);
            recyclerView.setLayoutManager(layoutManager);
            heading.setText(Mnu.get_topic(0));
            DataAdapter adapter = new DataAdapter(getApplicationContext(), Mnu.get_menu_titles());
            recyclerView.setAdapter(adapter);

        }



    }



    private void initViews() {
        recyclerView=(RecyclerView)findViewById(R.id.recyclerview);
        array_items=getResources().getStringArray(R.array.array_items);
        heading = (TextView)findViewById(R.id.heading_text);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(MainActivity.this,Launcher_Activity.class);
        startActivity(i);
    }
}
